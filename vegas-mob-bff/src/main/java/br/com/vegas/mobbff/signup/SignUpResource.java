package br.com.vegas.mobbff.signup;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import ps.vegas.core.exc.ObjAlreadyExistsException;
import ps.vegas.core.user.UserEntity;
import ps.vegas.core.user.UserService;
import br.com.vegas.mobbff.utils.CreatedObjectResponse;

@RestController
public class SignUpResource {

  public final static String SIGN_UP_PATH = "/sign-up";

  @Autowired
  private UserService userService;

  @PostMapping(path = SIGN_UP_PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public CreatedObjectResponse signUp(@Valid SignUpRequest request) throws ObjAlreadyExistsException {
    final UserEntity createdUser = userService.signUp(request.getName(), request.getEmail(), request.getPassword());
    return new CreatedObjectResponse(createdUser.getId());
  }

}
