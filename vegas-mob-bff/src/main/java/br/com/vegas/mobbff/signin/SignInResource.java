package br.com.vegas.mobbff.signin;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import ps.vegas.core.user.UserEntity;
import ps.vegas.core.user.UserService;
import br.com.vegas.mobbff.utils.CreatedObjectResponse;

@RestController
public class SignInResource {

  public final static String SIGN_IN_PATH = "/sign-in";

  @Autowired
  private UserService userService;

  @Autowired
  private SecretKeyProvider secretKeyProvider;

  @PostMapping(path = SIGN_IN_PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public CreatedObjectResponse signIn(SignInRequest request) {
    final UserEntity user = userService.signIn(request.getEmail(), request.getPassword()).orElse(null);
    if (user == null) {
      throw new IllegalStateException();
    }
    final String authId = new AuthenticatedUser(user.getId()).asJwtToken(secretKeyProvider, Optional.of(5));
    return new CreatedObjectResponse(authId);
  }
}
