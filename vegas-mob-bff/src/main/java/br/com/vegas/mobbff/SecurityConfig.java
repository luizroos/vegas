package br.com.vegas.mobbff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.vegas.mobbff.signin.AuthFilter;
import br.com.vegas.mobbff.signin.JwtAuthenticationProvider;
import br.com.vegas.mobbff.signin.SignInResource;
import br.com.vegas.mobbff.signup.SignUpResource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String ROLE_USER = "ROLE_USER";

  public static final String ROLE_ADMIN = "ROLE_ADMIN";

  @Autowired
  private AuthFilter jwtAuthFilter;

  @Autowired
  private JwtAuthenticationProvider jwtAuthenticationProvider;

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(jwtAuthenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // desabilita csrf
    http.csrf().disable();

    final ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests =
        http.authorizeRequests();

    // endpoints sem necessidade de autenticação
    authorizeRequests.antMatchers(SignInResource.SIGN_IN_PATH).permitAll()//
        .antMatchers(SignUpResource.SIGN_UP_PATH).permitAll() //
        .antMatchers("/_admin/health").permitAll() //
        .antMatchers("/v2/api-docs").permitAll() //
        .antMatchers("/webjars*/**").permitAll();

    // andpoints de administracao
    authorizeRequests.antMatchers("/swagger*/**").permitAll();
    authorizeRequests.antMatchers("/_admin/**").permitAll();
    // authorizeRequests.antMatchers("/swagger*/**").hasAuthority(SecurityConfig.ROLE_ADMIN).and().httpBasic();
    // authorizeRequests.antMatchers("/_admin/**").hasAuthority(SecurityConfig.ROLE_ADMIN).and().httpBasic();

    // endpoints que requerem usuário logado
    authorizeRequests.antMatchers("/**/*").hasAuthority(SecurityConfig.ROLE_USER)//
        .and().addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
  }
}
