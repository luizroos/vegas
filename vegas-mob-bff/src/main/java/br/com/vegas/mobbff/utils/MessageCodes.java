package br.com.vegas.mobbff.utils;

public final class MessageCodes {

  public static final String REQUIRED_FIELD = "required.field";

  public static final String INVALID_PATTERN = "invalid.pattern";

  public static final String INVALID_VALUE = "invalid.value";

  private MessageCodes() {
    throw new IllegalStateException();
  }
}
