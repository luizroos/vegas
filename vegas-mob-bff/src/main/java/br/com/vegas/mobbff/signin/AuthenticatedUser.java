package br.com.vegas.mobbff.signin;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import br.com.vegas.mobbff.BFFStarter;
import br.com.vegas.mobbff.SecurityConfig;

public class AuthenticatedUser implements Authentication {

  private static final long serialVersionUID = 1L;

  private final UUID userId;

  public AuthenticatedUser(UUID userId) {
    this.userId = Objects.requireNonNull(userId);
  }

  public UUID getUserId() {
    return userId;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.singletonList(new SimpleGrantedAuthority(SecurityConfig.ROLE_USER));
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getDetails() {
    return null;
  }

  @Override
  public Object getPrincipal() {
    return this;
  }

  @Override
  public boolean isAuthenticated() {
    return true;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

  }

  @Override
  public String getName() {
    return userId.toString();
  }

  public String asJwtToken(SecretKeyProvider secretKeyProvider, Optional<Integer> expiresIn) {
    byte[] secretKey;
    try {
      secretKey = secretKeyProvider.getKey();
      final Date expiration = expiresIn//
          .map(minutes -> ZonedDateTime.now().plusMinutes(minutes).toInstant())//
          .map(Date::from)//
          .orElse(null);

      return Jwts.builder()//
          .setSubject(userId.toString())//
          .setExpiration(expiration)//
          .setIssuer(BFFStarter.class.getPackage().getName())//
          .signWith(SignatureAlgorithm.HS512, secretKey).compact();
    } catch (URISyntaxException | IOException e) {
      throw new RuntimeException(e);
    }

  }

  public static AuthenticatedUser from(SecretKeyProvider secretKeyProvider, String jwtToken) throws URISyntaxException,
      IOException {
    final byte[] secretKey = secretKeyProvider.getKey();
    final Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
    final UUID userId = UUID.fromString(claims.getBody().getSubject());
    return new AuthenticatedUser(userId);
  }

}
