package br.com.vegas.mobbff.event;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ps.vegas.core.event.EventEntity;
import ps.vegas.core.event.EventOfferService;
import ps.vegas.core.event.EventRepository;
import ps.vegas.core.event.EventService;
import ps.vegas.core.event.PurchaseOfferEntity;
import ps.vegas.core.exc.UnknowEntityException;
import br.com.vegas.mobbff.signin.AuthenticatedUser;
import br.com.vegas.mobbff.utils.CreatedObjectResponse;

@RestController
public class EventResource {

  public final static String EVENTS_PATH = "/events";

  public final static String EVENT_PURCHASE_OFFERS_PATH = "/events/{eventId}/purchase-offers";

  public final static String EVENT_SALE_OFFERS_PATH = "/events/{eventId}/sale-offers";

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private EventService eventService;

  @Autowired
  private EventOfferService eventOfferService;

  @PostMapping(path = EVENTS_PATH, //
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, //
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public CreatedObjectResponse createEvent(@Valid CreateEventRequest request) throws UnknowEntityException {
    final AuthenticatedUser authUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication();
    final EventEntity createdEvent = eventService.createEvent(request.getName(), request.getEventDate(), authUser.getUserId());
    return new CreatedObjectResponse(createdEvent.getId());
  }

  @PostMapping(path = EVENT_PURCHASE_OFFERS_PATH, //
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, //
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public CreatedObjectResponse createPurchaseOffer(@PathVariable("eventId") UUID eventId, CreatePurchaseOfferRequest request)
      throws UnknowEntityException {
    final AuthenticatedUser authUser = (AuthenticatedUser) SecurityContextHolder.getContext().getAuthentication();
    final PurchaseOfferEntity createdOffer =
        eventOfferService.createPurchaseOffer(eventId, authUser.getUserId(), request.getAmount());
    return new CreatedObjectResponse(createdOffer.getId());
  }

  @GetMapping(path = EVENT_PURCHASE_OFFERS_PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public List<PurchaseOfferEntity> getPurchaseOffer(//
      @PathVariable("eventId") UUID eventId, //
      @RequestParam(name = "maxResults") int maxResults) throws UnknowEntityException {
    return eventRepository.getPurchaseOffers(eventId, maxResults);
  }
}
