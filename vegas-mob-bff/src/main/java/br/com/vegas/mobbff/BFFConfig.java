package br.com.vegas.mobbff;

import java.util.Arrays;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.web.filter.ShallowEtagHeaderFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import ps.vegas.core.CoreConfig;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import br.com.vegas.mobbff.utils.ProxyFilter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
@EnableSwagger2
@ComponentScan(basePackageClasses = BFFConfig.class)
@Import({CoreConfig.class})
public class BFFConfig implements WebMvcConfigurer {

  private ApiKey apiKey() {
    return new ApiKey("Authorization", "Authorization", "header");
  }

  /**
   * Configura o swagger, acesse via <HOST>:<PORTA>/swagger-ui.html
   */
  @Bean
  public Docket apiDoc() {
    return new Docket(DocumentationType.SWAGGER_2) //
        .securitySchemes(Arrays.asList(apiKey()))//
        .select()//
        .apis(RequestHandlerSelectors.basePackage(BFFConfig.class.getPackage().getName())) //
        .paths(PathSelectors.any()) //
        .build()//
        .apiInfo(new ApiInfoBuilder()//
            .title("Vegas Mob BFF")//
            .description("Vegas Mob BFF").build());
  }

  /**
   * Configura o object mapper do jackson, para parser e formatação de json
   */
  @Bean
  @Primary
  public ObjectMapper objectMapper() {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    return mapper;
  }

  /**
   * Configura filtro de etag
   */
  @Bean
  public Filter shallowEtagHeaderFilter() {
    // o client do dashboard do hystrix abre uma conexão para proxy.stream
    // que fica aberta escrevendo dados no dashboard. o filtro de etag não é para se aplicar
    // a essa requisição, por isso usa ProxyFilter com exclude do regex dessa URL
    return ProxyFilter.proxyTo(new ShallowEtagHeaderFilter()).excludeURI("^(?i).*(stream).*$");
  }

}
