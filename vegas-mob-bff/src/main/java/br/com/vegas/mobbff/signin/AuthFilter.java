package br.com.vegas.mobbff.signin;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthFilter implements Filter {

  @Autowired
  private SecretKeyProvider secretKeyProvider;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {}

  @Override
  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    final HttpServletRequest servletRequest = (HttpServletRequest) request;
    final String authorizationHeader = servletRequest.getHeader("Authorization");
    if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
      final String jwtToken = authorizationHeader.replaceAll("Bearer ", "");
      try {
        final AuthenticatedUser authenticatedUser = AuthenticatedUser.from(secretKeyProvider, jwtToken);
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
      } catch (Exception e) {
        SecurityContextHolder.getContext().setAuthentication(null);
      }
    }
    chain.doFilter(request, response);
  }

}
