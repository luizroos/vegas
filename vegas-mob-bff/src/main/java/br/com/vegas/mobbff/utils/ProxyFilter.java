package br.com.vegas.mobbff.utils;

import java.io.IOException;
import java.util.function.Function;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtro que basicamente faz proxy para outro filtro suportando regex como blacklist para ser
 * aplicado na requestURI. Útil quando você quer aplicar um filtro para todas requisições, exceto
 * algumas URIs.
 * <p>
 * Use ProxyFilter.proxyTo(SEU FILTRO).exclude(regex);
 * */
public class ProxyFilter extends AbstractHttpFilter {

  private static final Function<HttpServletRequest, Boolean> APPLY_ALL_FN = new Function<HttpServletRequest, Boolean>() {
    @Override
    public Boolean apply(HttpServletRequest t) {
      return true;
    }
  };

  private final Filter proxyTo;

  private Function<HttpServletRequest, Boolean> applyFn = APPLY_ALL_FN;

  private ProxyFilter(Filter proxyTo) {
    if (proxyTo == null) {
      throw new NullPointerException();
    }
    this.proxyTo = proxyTo;
  }

  /**
   * Exclui o filtro para URIs que fazem match com qualquer regex informada
   * */
  public Filter excludeURI(final String... regex) {
    if (regex != null) {
      applyToRequest(new Function<HttpServletRequest, Boolean>() {
        @Override
        public Boolean apply(HttpServletRequest req) {
          final String requestURI = req.getRequestURI();
          for (String r : regex) {
            if (requestURI.matches(r)) {
              return false;
            }
          }
          return true;
        }
      });
    }
    return this;
  }

  /**
   * Seta a função que determina se o filtro deve ou não ser aplicado para o request
   * */
  public Filter applyToRequest(Function<HttpServletRequest, Boolean> applyFn) {
    if (applyFn == null) {
      throw new IllegalArgumentException();
    }
    this.applyFn = applyFn;
    return this;
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    proxyTo.init(filterConfig);
  }

  @Override
  public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException,
      ServletException {
    if (applyFn.apply(request)) {
      proxyTo.doFilter(request, response, chain);
    } else {
      chain.doFilter(request, response);
    }
  }

  @Override
  public void destroy() {
    proxyTo.destroy();
  }

  public static ProxyFilter proxyTo(Filter filter) {
    return new ProxyFilter(filter);
  }

}
