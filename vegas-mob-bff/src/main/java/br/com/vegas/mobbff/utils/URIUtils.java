package br.com.vegas.mobbff.utils;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.util.MultiValueMap;

public class URIUtils {

  public static URI getURI(String uri, MultiValueMap<String, String> params) throws URISyntaxException {
    final URIBuilder uriBuilder = new URIBuilder().setPath(uri);
    params.toSingleValueMap().forEach(uriBuilder::addParameter);
    return uriBuilder.build();
  }

}
