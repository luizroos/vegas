package br.com.vegas.mobbff.utils;

import java.util.UUID;

public class CreatedObjectResponse {

  private String id;

  public CreatedObjectResponse(UUID id) {
    this.id = id.toString();
  }

  public CreatedObjectResponse(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

}
