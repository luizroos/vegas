package br.com.vegas.mobbff.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Conveniente implementação de Filter que já faz a checagem e casting de HttpServletRequest e
 * HttpServletResponse. Implementa por default também init e destroy sem fazer nada
 * */
public abstract class AbstractHttpFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    if (!(request instanceof HttpServletRequest) || !(response instanceof HttpServletResponse)) {
      throw new ServletException("Não é HTTP request");
    }
    doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
  }

  @Override
  public void destroy() {}

  protected abstract void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException;

}
