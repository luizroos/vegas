package br.com.vegas.mobbff.event;

import java.time.ZonedDateTime;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class CreateEventRequest {

  @NotEmpty
  private String name;

  // ex 2019-10-31T01:30:00.000-05:00
  @DateTimeFormat(iso = ISO.DATE_TIME)
  @NotNull
  @Future
  private ZonedDateTime eventDate;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ZonedDateTime getEventDate() {
    return eventDate;
  }

  public void setEventDate(ZonedDateTime eventDate) {
    this.eventDate = eventDate;
  }

}
