package br.com.vegas.mobbff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({BFFConfig.class})
public class BFFStarter {

  public static void main(String[] args) throws Exception {
    SpringApplication.run(BFFStarter.class, args);
  }

}
