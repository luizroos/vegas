package br.com.vegas.mobbff.event;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CreatePurchaseOfferRequest {

  @NotNull
  @Min(value = 1)
  private Long amount;

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

}
