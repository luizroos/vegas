package ps.vegas.core.exc;

import java.util.Objects;

public class UnknowEntityException extends Exception {

  private static final long serialVersionUID = 1L;

  private final Class<?> entityClass;

  private final Object entityId;

  public UnknowEntityException(Class<?> entityClass, Object entityId) {
    this.entityClass = Objects.requireNonNull(entityClass);
    this.entityId = Objects.requireNonNull(entityId);
  }

  public Class<?> getEntityClass() {
    return entityClass;
  }

  public Object getEntityId() {
    return entityId;
  }

}
