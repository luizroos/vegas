package ps.vegas.core;

public class Profiles {

  public static final String TEST_PROFILE = "test";

  public static final String NOT_TEST_PROFILE = "!test";

  public static final String DEV_PROFILE = "dev";

  public static final String NOT_DEV_PROFILE = "!" + DEV_PROFILE;

  private Profiles() {
    throw new AssertionError();
  }

}
