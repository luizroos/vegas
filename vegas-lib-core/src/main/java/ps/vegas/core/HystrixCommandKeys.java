package ps.vegas.core;

public class HystrixCommandKeys {

  public static final String CHARGEBACK_GET_CBK_BY_CODE = HystrixGroupKeys.CHARGEBACK + ".getCbkByCode";

  public static final String PAYMENT_FIND_PAYMENTS = HystrixGroupKeys.PAYMENT + ".findPayments";

  private HystrixCommandKeys() {
    throw new UnsupportedOperationException();
  }

}
