package ps.vegas.core;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.collect.Sets;

@Configuration
public class RestTemplateConfig {

  @Value("${httpclient.readTimeout}")
  private int readTimeout;

  @Value("${httpclient.connectTimeout}")
  private int connectTimeout;

  @Profile(Profiles.DEV_PROFILE)
  @Bean
  public HttpComponentsClientHttpRequestFactory devClientHttpRequestFactory() {
    // setSSLHostnameVerifier necessario para o profile de dev que acessa o monolitico pelo mesos
    // com um dominio diferente do configurado no certificado
    final CloseableHttpClient client = HttpClients.custom()//
        .setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

    final HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(client);
    factory.setReadTimeout(readTimeout);
    factory.setConnectTimeout(connectTimeout);
    return factory;
  }

  @Profile(Profiles.NOT_DEV_PROFILE)
  @Bean
  public SimpleClientHttpRequestFactory clientHttpRequestFactory() {
    SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
    factory.setReadTimeout(readTimeout);
    factory.setConnectTimeout(connectTimeout);
    return factory;
  }

  /**
   * Algumas apis do pagseguro parseiam json com camel case e outras snake case, por isso
   * registramos 2 converters do jackson, um para snake case e outro para camel case. A forma para
   * indicar como parsear o objeto é feita por anotações no objeto response da chamada a API. Existe
   * duas anotações: {@link SnakeCasePropertyNamingStrategy} e
   * {@link CamelCasePropertyNamingStrategy}, veja um exemplo de uso {@link GetCbkByCodeResponse}
   * 
   * */
  @Bean
  public RestTemplate restTemplate(ClientHttpRequestFactory clientHttpRequestFactory) {

    final MappingJackson2HttpMessageConverter jacksonSnakeCase = new MappingJackson2HttpMessageConverter();
    jacksonSnakeCase.setObjectMapper(createObjectMapper(PropertyNamingStrategy.SNAKE_CASE));

    final MappingJackson2HttpMessageConverter jacksonCamelCase = new MappingJackson2HttpMessageConverter();
    jacksonCamelCase.setObjectMapper(createObjectMapper(PropertyNamingStrategy.LOWER_CAMEL_CASE));

    return new RestTemplateBuilder()//
        .requestFactory(() -> clientHttpRequestFactory)//
        .messageConverters(Sets.newHashSet(//
            new AnnRequiredHttpConverter(jacksonSnakeCase, SnakeCasePropertyNamingStrategy.class), //
            new AnnRequiredHttpConverter(jacksonCamelCase, CamelCasePropertyNamingStrategy.class)))//
        .build();
  }

  private ObjectMapper createObjectMapper(PropertyNamingStrategy strategy) {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.setPropertyNamingStrategy(strategy);
    return mapper;
  }

  /**
   * Indica que o json deve ser parseado com PropertyNamingStrategy.SNAKE_CASE
   * */
  @Target({ElementType.TYPE})
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface SnakeCasePropertyNamingStrategy {

  }

  /**
   * Indica que o json deve ser parseado com PropertyNamingStrategy.LOWER_CAMEL_CASE
   * */
  @Target({ElementType.TYPE})
  @Retention(RetentionPolicy.RUNTIME)
  public static @interface CamelCasePropertyNamingStrategy {

  }

  private static class AnnRequiredHttpConverter implements HttpMessageConverter<Object> {

    private final HttpMessageConverter<Object> delegateTo;

    private final Class<? extends Annotation> requiredAnn;

    public AnnRequiredHttpConverter(HttpMessageConverter<Object> delegateTo, Class<? extends Annotation> requiredAnn) {
      this.delegateTo = delegateTo;
      this.requiredAnn = requiredAnn;
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
      return clazz.isAnnotationPresent(requiredAnn) && delegateTo.canRead(clazz, mediaType);
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
      return clazz.isAnnotationPresent(requiredAnn) && delegateTo.canWrite(clazz, mediaType);
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
      return delegateTo.getSupportedMediaTypes();
    }

    @Override
    public Object read(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException,
        HttpMessageNotReadableException {
      return delegateTo.read(clazz, inputMessage);
    }

    @Override
    public void write(Object t, MediaType contentType, HttpOutputMessage outputMessage) throws IOException,
        HttpMessageNotWritableException {
      delegateTo.write(t, contentType, outputMessage);
    }

  }

}
