package ps.vegas.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile(Profiles.NOT_TEST_PROFILE)
public class RestClientConfig {

}
