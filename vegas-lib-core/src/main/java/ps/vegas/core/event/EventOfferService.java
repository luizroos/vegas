package ps.vegas.core.event;

import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ps.vegas.core.exc.UnknowEntityException;
import ps.vegas.core.user.UserEntity;
import ps.vegas.core.user.UserService;

@Transactional
@Service
public class EventOfferService {

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private EventService eventService;

  @Autowired
  private UserService userService;

  public PurchaseOfferEntity createPurchaseOffer(UUID eventId, UUID userId, long amount) throws UnknowEntityException {
    final EventEntity event = eventService.findRequiredEventById(eventId);
    final UserEntity user = userService.findRequiredUserById(userId);
    return eventRepository.createPurchaseOffer(event, user, amount);
  }
}
