package ps.vegas.core.event;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import ps.vegas.core.user.UserEntity;

@Entity(name = PurchaseOfferEntity.ENTITY_NAME)
public class PurchaseOfferEntity {

  public final static String ENTITY_NAME = "PurchaseOffer";

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "id_purchase_offer", updatable = false, nullable = false)
  private UUID id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "id_event", nullable = false)
  private EventEntity event;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "id_user", nullable = false)
  private UserEntity user;

  @Column(name = "num_amount", nullable = false)
  private long amount;

  @Column(name = "dat_offer", nullable = false)
  private ZonedDateTime offerDate;

  @Deprecated
  public PurchaseOfferEntity() {}

  public PurchaseOfferEntity(EventEntity event, UserEntity user, long amount) {
    this.event = Objects.requireNonNull(event);
    this.user = Objects.requireNonNull(user);
    this.amount = Objects.requireNonNull(amount);
    this.offerDate = ZonedDateTime.now();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public EventEntity getEvent() {
    return event;
  }

  public void setEvent(EventEntity event) {
    this.event = event;
  }

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

  public long getAmount() {
    return amount;
  }

  public void setAmount(long amount) {
    this.amount = amount;
  }

}
