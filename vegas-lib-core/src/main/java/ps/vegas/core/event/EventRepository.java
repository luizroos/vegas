package ps.vegas.core.event;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import ps.vegas.core.user.UserEntity;

public interface EventRepository {

  List<PurchaseOfferEntity> getPurchaseOffers(UUID eventId, int maxResults);

  PurchaseOfferEntity createPurchaseOffer(EventEntity event, UserEntity user, long amount);

  EventEntity createEvent(String name, ZonedDateTime eventDate, UserEntity user);

  Optional<EventEntity> findEventById(UUID id);

}
