package ps.vegas.core.event;

import java.time.ZonedDateTime;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ps.vegas.core.exc.UnknowEntityException;
import ps.vegas.core.user.UserEntity;
import ps.vegas.core.user.UserService;

@Transactional
@Service
public class EventService {

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private UserService userService;

  public EventEntity findRequiredEventById(UUID id) throws UnknowEntityException {
    return eventRepository.findEventById(id).orElseThrow(() -> new UnknowEntityException(EventEntity.class, id));
  }

  public EventEntity createEvent(String name, ZonedDateTime eventDate, UUID userId) throws UnknowEntityException {
    final UserEntity user = userService.findRequiredUserById(userId);
    return eventRepository.createEvent(name, eventDate, user);
  }

}
