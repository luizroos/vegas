package ps.vegas.core.event;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import ps.vegas.core.user.UserEntity;

@Entity(name = EventEntity.ENTITY_NAME)
public class EventEntity {

  public final static String ENTITY_NAME = "Event";

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "id_event", updatable = false, nullable = false)
  private UUID id;

  @Column(name = "nam_event", nullable = false)
  private String name;

  @Column(name = "dat_event", nullable = false)
  private ZonedDateTime eventDate;

  @Column(name = "dat_creation", nullable = false)
  private ZonedDateTime createdAt;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "id_user", nullable = false)
  private UserEntity user;

  @Deprecated
  public EventEntity() {}

  public EventEntity(String name, ZonedDateTime eventDate, UserEntity user) {
    this.name = Objects.requireNonNull(name);
    this.user = Objects.requireNonNull(user);
    this.eventDate = Objects.requireNonNull(eventDate);
    this.createdAt = ZonedDateTime.now();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ZonedDateTime getEventDate() {
    return eventDate;
  }

  public void setEventDate(ZonedDateTime eventDate) {
    this.eventDate = eventDate;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

}
