package ps.vegas.core.event;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ps.vegas.core.user.UserEntity;

@Transactional
@Repository
public class JPAEventRepositoy implements EventRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public EventEntity createEvent(String name, ZonedDateTime eventDate, UserEntity user) {
    final EventEntity event = new EventEntity(name, eventDate, user);
    entityManager.persist(event);
    return event;
  }

  @Override
  public Optional<EventEntity> findEventById(UUID id) {
    return Optional.ofNullable(entityManager.find(EventEntity.class, id));
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<PurchaseOfferEntity> getPurchaseOffers(UUID eventId, int maxResults) {
    final StringBuilder jql = new StringBuilder();
    jql.append(" select e from ");
    jql.append(PurchaseOfferEntity.ENTITY_NAME);
    jql.append(" e where e.event.id = :eventId");
    jql.append(" order by e.amount desc ");

    Query query = entityManager.createQuery(jql.toString());
    query.setParameter("eventId", eventId);
    query.setMaxResults(maxResults);

    return query.getResultList();
  }

  @Override
  public PurchaseOfferEntity createPurchaseOffer(EventEntity event, UserEntity user, long amount) {
    final PurchaseOfferEntity offer = new PurchaseOfferEntity(event, user, amount);
    entityManager.persist(offer);
    return offer;
  }
}
