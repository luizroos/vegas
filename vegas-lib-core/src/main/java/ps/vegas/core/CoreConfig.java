package ps.vegas.core;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableCircuitBreaker
@EnableHystrixDashboard
@EnableJpaRepositories
@EnableTransactionManagement
@EntityScan
@ComponentScan(basePackageClasses = { CoreConfig.class })
@PropertySource(ignoreResourceNotFound = false, value = "classpath:core.application.properties")
@PropertySource(ignoreResourceNotFound = true, value = "classpath:core.application-${spring.profiles.active}.properties")
public class CoreConfig {

}
