package ps.vegas.core.client;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class RestClient {

  private final static Logger LOGGER = LoggerFactory.getLogger(RestClient.class);

  private final RestTemplate restTemplate;

  private final String apiHost;

  public RestClient(RestTemplate restTemplate, String apiHost) {
    this.restTemplate = Objects.requireNonNull(restTemplate);
    this.apiHost = Objects.requireNonNull(apiHost);
  }

  public <T> T doGet(String endpoint, Class<T> responseType) {
    final String url = String.format("%s%s", apiHost, endpoint);
    try {
      final ResponseEntity<T> response = restTemplate.getForEntity(url, responseType);
      if (response != null) {
        LOGGER.debug("m=doGet,responseStatus={},tClass={}", response.getStatusCodeValue(), responseType.getSimpleName());
        return response.getBody();
      }
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
    }
    LOGGER.debug("m=doGet,response=null,tClass={}", responseType.getSimpleName());
    return null;
  }

  public <T> T doPost(String endpoint, Object request, Class<T> responseType) {
    final String url = String.format("%s%s", apiHost, endpoint);
    try {
      final ResponseEntity<T> response = restTemplate.postForEntity(url, request, responseType);
      if (response != null) {
        LOGGER.debug("m=doPost,responseStatus={},tClass={}", response.getStatusCodeValue(), responseType.getSimpleName());
        return response.getBody();
      }
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
    }
    LOGGER.debug("m=doGet,response=null,tClass={}", responseType.getSimpleName());
    return null;
  }

}
