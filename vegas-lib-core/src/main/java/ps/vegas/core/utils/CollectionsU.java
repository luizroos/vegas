package ps.vegas.core.utils;

import java.util.List;
import java.util.Optional;

public class CollectionsU {

  public static <T> Optional<T> getUnique(List<T> list) {
    if (list.size() == 0) {
      return Optional.empty();
    }
    if (list.size() > 1) {
      throw new IllegalStateException();
    }
    return Optional.of(list.get(0));
  }

}
