package ps.vegas.core.utils;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class PublicFormats {

  private static DateTimeFormatter DATE_TIME_FORMATER = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

  private static DateTimeFormatter DATE_FORMATER = DateTimeFormatter.ISO_DATE;

  public static String formatDateTime(ZonedDateTime date) {
    return DATE_TIME_FORMATER.format(date);
  }

  public static String formatDate(LocalDate date) {
    return DATE_FORMATER.format(date);
  }

}
