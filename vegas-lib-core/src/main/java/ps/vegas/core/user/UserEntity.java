package ps.vegas.core.user;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = UserEntity.ENTITY_NAME)
@Table(indexes = {//
    @Index(columnList = "des_email", unique = true)})
public class UserEntity {

  public final static String ENTITY_NAME = "User";

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "id_user", updatable = false, nullable = false)
  private UUID id;

  @Column(name = "nam_user", nullable = false)
  private String name;

  @Column(name = "des_email", nullable = false)
  private String email;

  @Column(name = "des_password", nullable = false)
  private String hashedPassword;

  @Column(name = "dat_creation", nullable = false)
  private ZonedDateTime createdAt;

  @Deprecated
  public UserEntity() {}

  public UserEntity(String name, String email, String hashedPassword) {
    this.name = Objects.requireNonNull(name);
    this.email = Objects.requireNonNull(email);
    this.hashedPassword = Objects.requireNonNull(hashedPassword);
    this.createdAt = ZonedDateTime.now();
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getHashedPassword() {
    return hashedPassword;
  }

  public void setHashedPassword(String hashedPassword) {
    this.hashedPassword = hashedPassword;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

}
