package ps.vegas.core.user;

import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ps.vegas.core.exc.ObjAlreadyExistsException;
import ps.vegas.core.exc.UnknowEntityException;

@Transactional
@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  public UserEntity findRequiredUserById(UUID id) throws UnknowEntityException {
    return userRepository.findUserById(id).orElseThrow(() -> new UnknowEntityException(UserEntity.class, id));
  }

  public UserEntity signUp(String name, String email, String password) throws ObjAlreadyExistsException {
    if (userRepository.findUserByEmail(email).isPresent()) {
      throw new ObjAlreadyExistsException();
    }
    return userRepository.createUser(name, email, hashPass(password));
  }

  public Optional<UserEntity> signIn(String email, String password) {
    return userRepository.findUserByEmail(email)//
        .filter(u -> u.getHashedPassword().equals(hashPass(password)));
  }

  private static String hashPass(String password) {
    return DigestUtils.sha512Hex(password);
  }
}
