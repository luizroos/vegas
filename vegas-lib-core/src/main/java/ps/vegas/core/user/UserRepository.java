package ps.vegas.core.user;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository {

  Optional<UserEntity> findUserById(UUID id);

  UserEntity createUser(String name, String email, String hashedPassword);

  Optional<UserEntity> findUserByEmail(String email);

}
