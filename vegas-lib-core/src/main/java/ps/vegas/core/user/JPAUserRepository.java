package ps.vegas.core.user;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import ps.vegas.core.utils.CollectionsU;

@Transactional
@Repository
public class JPAUserRepository implements UserRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public Optional<UserEntity> findUserById(UUID id) {
    return Optional.ofNullable(entityManager.find(UserEntity.class, id));
  }

  @Override
  @SuppressWarnings("unchecked")
  public Optional<UserEntity> findUserByEmail(String email) {
    final StringBuilder jql = new StringBuilder();
    jql.append(" select e from ");
    jql.append(UserEntity.ENTITY_NAME);
    jql.append(" e where e.email = :email");

    final Query query = entityManager.createQuery(jql.toString());
    query.setParameter("email", email);

    final List<UserEntity> users = query.getResultList();
    return CollectionsU.getUnique(users);
  }

  @Override
  public UserEntity createUser(String name, String email, String hashedPassword) {
    final UserEntity user = new UserEntity(name, email, hashedPassword);
    entityManager.persist(user);
    return user;
  }
}
