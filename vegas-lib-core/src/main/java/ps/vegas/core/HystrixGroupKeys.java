package ps.vegas.core;

public class HystrixGroupKeys {

  public static final String CHARGEBACK = "chargeback";

  public static final String PAYMENT = "payment";

  private HystrixGroupKeys() {
    throw new UnsupportedOperationException();
  }

}
