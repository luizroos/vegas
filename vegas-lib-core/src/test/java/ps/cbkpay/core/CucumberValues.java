package ps.cbkpay.core;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CucumberValues {

  private static final Pattern DATE_REL_PATTERN = Pattern.compile("(\\d+) (dias|horas|minutos|segundos) (atrás|para frente)");

  public CucumberValues() {
    throw new UnsupportedOperationException();
  }

  private static LocalDateTime parseStringDate(String s, String dateFormat) {
    if (s.equals("hoje") || s.equals("agora")) {
      return LocalDateTime.now();
    } else if (s.equals("ontem")) {
      return LocalDateTime.now().minusDays(1);
    } else {
      final Matcher m = DATE_REL_PATTERN.matcher(s);
      if (m.find()) {
        final String timeType = m.group(2);
        int amount = Integer.parseInt(m.group(1));
        final String direc = m.group(3);
        if (direc.equals("atrás")) {
          amount = -amount;
        }
        if (timeType.equals("dias")) {
          return LocalDateTime.now().plusDays(amount);
        } else if (timeType.equals("horas")) {
          return LocalDateTime.now().plusHours(amount);
        } else if (timeType.equals("minutos")) {
          return LocalDateTime.now().plusMinutes(amount);
        } else if (timeType.equals("segundos")) {
          return LocalDateTime.now().plusSeconds(amount);
        } else {
          throw new IllegalArgumentException();
        }
      } else {
        try {
          return LocalDateTime.parse(s, DateTimeFormatter.ofPattern(dateFormat));
        } catch (Exception e) {
          throw new IllegalStateException(e);
        }
      }
    }
  }

  public static Optional<LocalDateTime> optDateValue(String value, String dateFormat) {
    return optStringValue(value).map(s -> parseStringDate(s, dateFormat));
  }

  public static LocalDateTime dateValue(String value, String dateFormat) {
    return optDateValue(value, dateFormat).orElse(null);
  }

  public static String stringValue(String value) {
    return optStringValue(value).orElse(null);
  }

  public static Long longValue(String value) {
    return optLongValue(value).orElse(null);
  }

  public static Integer intValue(String value) {
    return optIntValue(value).orElse(null);
  }

  public static boolean boolValue(String value) {
    return optStringValue(value).map(Boolean::valueOf).orElse(false);
  }

  public static Optional<String> optStringValue(String value) {
    if (value != null && !"null".equals(value)) {
      return Optional.of(value);
    }
    return Optional.empty();
  }

  public static Optional<Long> optLongValue(String value) {
    return optStringValue(value).map(Long::valueOf);
  }

  public static Optional<Integer> optIntValue(String value) {
    return optStringValue(value).map(Integer::valueOf);
  }

}
